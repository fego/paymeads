<script language = "Javascript">
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;
var cCode = 0;
function isInteger(s)
{ var i;
for (i = 0; i < s.length; i++)
{ 
var c = s.charAt(i);
if (((c < "0") || (c > "9"))) return false;
}
return true;
}
function trim(s)
{ var i;
var returnString = "";
for (i = 0; i < s.length; i++)
{ 
var c = s.charAt(i);
if (c != " ") returnString += c;
}
return returnString;
}
function stripCharsInBag(s, bag)
{ var i;
var returnString = "";
for (i = 0; i < s.length; i++)
{ 
var c = s.charAt(i);
if (bag.indexOf(c) == -1) returnString += c;
}
return returnString;
}
function checkInternationalPhone(strPhone){
var bracket=3
strPhone=trim(strPhone)
if(strPhone.indexOf("+")>1) return false
if(strPhone.indexOf("-")!=-1)bracket=bracket+1
if(strPhone.indexOf("(")!=-1 && strPhone.indexOf("(")>bracket)return false
var brchr=strPhone.indexOf("(")
if(strPhone.indexOf("(")!=-1 && strPhone.charAt(brchr+2)!=")")return false
if(strPhone.indexOf("(")==-1 && strPhone.indexOf(")")!=-1)return false
s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}
</script>
<script type="text/javascript">
function validate()
{
	var valid=document.form;
	var iChars = "{}#^=|'<>~`\$@*;";
	var erColor="#FBFFFF";
	var digits = "0123456789";
	var Phone=document.form.phone;
			
	if(IsEmpty(valid.name))
	{
		alert("Please enter your name");
		valid.name.style.background= erColor;
		valid.name.focus();
		return false;
	}
	else
	{
		valid.name.style.background= 'White';
		valid.name.focus();
	}
	for (var i = 0; i < valid.name.value.length; i++)
	{
		if (iChars.indexOf(valid.name.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.name.style.background= erColor;
			valid.name.focus();
			return false;
		}
	}
	for (var i = 0; i < valid.name.value.length; i++)
	{
		if (digits.indexOf(valid.name.value.charAt(i)) != -1)
		{
			alert ("Please avoid numbers");
			valid.name.style.background= erColor;
			valid.name.focus();
			return false;
		}
	}
	if(IsEmpty(valid.sex))
	{
		alert("Please choose your sex");
		valid.sex.style.background= erColor;
		valid.sex.focus();
		return false;
	}
	else
	{
		valid.sex.style.background= 'White';
		valid.sex.focus();
	}
	if(Phone.value=='')
	{
		alert("Please enter your phone number");
		Phone.style.background= erColor;
		Phone.focus();
		return false;
	}
	else
	{
		Phone.style.background= 'White';
		Phone.focus();
	}
	if(Phone.value!='')
	{
		if (checkInternationalPhone(Phone.value)==false)
		{
			alert("Please enter a valid contact number");
			Phone.style.background=erColor;
			Phone.focus();
			return false;
		}
	}
	else
	{
		Phone.style.background= 'White';
		Phone.focus();
	}
	if(IsEmpty(valid.email))
	{
		alert("Please enter your email");
		valid.email.style.background= erColor;
		valid.email.focus();
		return false;
	}
	else
	{
		valid.email.style.background= 'White';
		valid.email.focus();
	}
	if (valid.email.value!="")
	{
		if (valid.email.value.match(/[a-zA-Z0-9]+\@[a-zA-Z0-9-]+(\.(a-zA-Z0-9]{2}|[a-zA-Z0-9]{2}))+/)==null)
		{
			alert ("Please enter a valid email-address");
			valid.email.style.background= erColor;
			valid.email.focus();
			return false;
		}
		else
		{
			valid.email.style.background= 'White';
			valid.email.focus();
		}
	}
	if(IsEmpty(valid.address))
	{
		alert("Please enter your address");
		valid.address.style.background= erColor;
		valid.address.focus();
		return false;
	}
	else
	{
		valid.address.style.background= 'White';
		valid.address.focus();
	}
	for (var i = 0; i < valid.address.value.length; i++)
	{
		if (iChars.indexOf(valid.address.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.address.style.background= erColor;
			valid.address.focus();
			return false;
		}
	}
	if(IsEmpty(valid.car))
	{
		alert("Please enter your car name");
		valid.car.style.background= erColor;
		valid.car.focus();
		return false;
	}
	else
	{
		valid.car.style.background= 'White';
		valid.car.focus();
	}
	for (var i = 0; i < valid.car.value.length; i++)
	{
		if (iChars.indexOf(valid.car.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.car.style.background= erColor;
			valid.car.focus();
			return false;
		}
	}
	if(IsEmpty(valid.model))
	{
		alert("Please enter your car model");
		valid.model.style.background= erColor;
		valid.model.focus();
		return false;
	}
	else
	{
		valid.model.style.background= 'White';
		valid.model.focus();
	}
	for (var i = 0; i < valid.model.value.length; i++)
	{
		if (iChars.indexOf(valid.model.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.model.style.background= erColor;
			valid.model.focus();
			return false;
		}
	}
	if(IsEmpty(valid.carno))
	{
		alert("Please enter your car number");
		valid.carno.style.background= erColor;
		valid.carno.focus();
		return false;
	}
	else
	{
		valid.carno.style.background= 'White';
		valid.carno.focus();
	}
	for (var i = 0; i < valid.carno.value.length; i++)
	{
		if (iChars.indexOf(valid.carno.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.carno.style.background= erColor;
			valid.carno.focus();
			return false;
		}
	}
	if(IsEmpty(valid.colour))
	{
		alert("Please choose your car colour");
		valid.colour.style.background= erColor;
		valid.colour.focus();
		return false;
	}
	else
	{
		valid.colour.style.background= 'White';
		valid.colour.focus();
	}
	if(IsEmpty(valid.door))
	{
		alert("Please choose your number of car doors");
		valid.door.style.background= erColor;
		valid.door.focus();
		return false;
	}
	else
	{
		valid.door.style.background= 'White';
		valid.door.focus();
	}
	if(IsEmpty(valid.mile))
	{
		alert("Please choose miles driven per year");
		valid.mile.style.background= erColor;
		valid.mile.focus();
		return false;
	}
	else
	{
		valid.mile.style.background= 'White';
		valid.mile.focus();
	}
	if(IsEmpty(valid.duration))
	{
		alert("Please choose duration");
		valid.duration.style.background= erColor;
		valid.duration.focus();
		return false;
	}
	else
	{
		valid.duration.style.background= 'White';
		valid.duration.focus();
	}
	if(IsEmpty(valid.rate))
	{
		alert("Please enter your car rate per day");
		valid.rate.style.background= erColor;
		valid.rate.focus();
		return false;
	}
	else
	{
		valid.rate.style.background= 'White';
		valid.rate.focus();
	}
	if(isNaN(valid.rate.value))
    {
		alert("Please enter numbers only");
		valid.rate.style.background=erColor;
		valid.rate.focus();
		return false;
    }
  	else
	{
		valid.rate.style.background= 'White';
		valid.rate.focus();
	}

	function IsEmpty(obj)
	{
		var objValue;
		objValue = obj.value.replace(/\s+$/,"");
		if(objValue.length == 0)
		{return true;}
		else{return false;}
	}
}
</script>
<?php
include("db.php");
if($_POST['submit']!='')
{
$name=$_POST['name'];
$sex=$_POST['sex'];
$phone=$_POST['phone'];
$email=$_POST['email'];
$address=$_POST['address'];
$car=$_POST['car'];
$model=$_POST['model'];
$colour=$_POST['colour'];
$carno=$_POST['carno'];
$door=$_POST['door'];
$mile=$_POST['mile'];
$duration=$_POST['duration'];
$rate=$_POST['rate'];
$date=date("d-m-Y");
// Insert Car Query Starts
	$qry="insert into register (Name,Sex,Phone,Email,Address,Car,Model,Colour,Number,Doors,Miles,Duration,Rate,Date) values('$name','$sex','$phone','$email','$address','$car','$model','$colour','$carno','$door','$mile','$duration','$rate','$date')";
	$res=mysql_query($qry);
	printf("<script>location.href='register.php?active=RE&success=1'</script>");
}

?>
<header id="head" class="secondary">
    <div class="container">
		<h1>Register Your Car</h1>
	</div>
</header>
	<!-- container -->
	<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<p>&nbsp;</p>
                        <?php
                        if($_REQUEST['success']==1)
                        {
                        echo  '<div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Your car was register successfully!! Get in touch with paymeads.in team!!
                        </div>';
                        }
                        ?>
						<form name="form" method="post" action="register.php?active=RE" class="form-light mt-20" role="form" onsubmit="return validate();">
							<div class="row">
								<div class="col-md-12">
									<label class="text-primary">Personal Details</label>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="name" class="form-control" placeholder="* Name" maxlength="30">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<select name="sex" class="form-control">
                                        <option selected="selected" value="">* Sex</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="phone" class="form-control" placeholder="* Phone" maxlength="30">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="email" class="form-control" placeholder="* Email" maxlength="30">
									</div>
								</div>
							</div>
							<div class="form-group">
								<textarea name="address" class="form-control" id="address" placeholder="* Address" style="height:80px;" maxlength="750"></textarea>
							</div>
                            <div class="row">
								<div class="col-md-12">
									<label class="text-primary">Car Details</label>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="car" class="form-control" placeholder="* Car Name" maxlength="20">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="model" class="form-control" placeholder="* Model" maxlength="10">
									</div>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
                                    	<input type="text" name="carno" class="form-control" placeholder="* Car No" maxlength="15">
									</div>
								</div>								
                                <div class="col-md-6">
									<div class="form-group">
                                    	<select name="colour" class="form-control">
                                        <option value="" selected="selected">* Colour</option>
                                        <option value="Black">Black</option>
                                        <option value="Blue">Blue</option>
                                        <option value="Brown">Brown</option>
                                        <option value="Bronze">Bronze</option>
                                        <option value="Grey">Grey</option>
                                        <option value="Green">Green</option>
                                        <option value="Orange">Orange</option>
                                        <option value="Pink">Pink</option>
                                        <option value="Purple">Purple</option>
                                        <option value="Red">Red</option>
                                        <option value="Gold">Gold</option>
                                        <option value="Silver">Silver</option>
                                        <option value="White">White</option>
                                        <option value="Yellow">Yellow</option>
                                        </select>	
									</div>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<select name="door" class="form-control">
                                        <option value="" selected="selected">* Number of Doors</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        </select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<select name="mile" class="form-control">
                                        <option value="" selected="selected">* Miles driven per year</option>
                                        <option value="Under 3000 miles">Under 3000 miles</option>
                                        <option value="3001-5000 miles">3001-5000 miles</option>
                                        <option value="5001-7000 miles">5001-7000 miles</option>
                                        <option value="7001-9000 mile">7001-9000 miles</option>
                                        <option value="Above 9000 miles">Above 9000 miles</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-12">
									<label class="text-primary">Payment Details</label>
								</div>
							</div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<select name="duration" class="form-control">
                                        <option value="" selected="selected">* Ad Duration</option>
                                        <option value="1 Month">1 Month</option>
                                        <option value="3 Month">3 Month</option>
                                        <option value="6 Month">6 Month</option>
                                        <option value="1 Year">1 Year</option>
                                        </select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="rate" class="form-control" placeholder="* Rate per day" maxlength="5">
									</div>
								</div>
							</div>
							<div class="form-group" align="right"><input name="submit" type="submit" class="btn btn-two" value="Register"></div><p><br/></p>
						</form>
					</div>
				</div>
			</div>
	<!-- /container -->