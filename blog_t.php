	<header id="head" class="secondary">
            <div class="container">
                    <h1>Blog</h1>
                </div>
    </header>

	<!-- container -->
	<section class="container">

		<div class="row">

			<!-- Article main content -->
			<article class="col-md-8 maincontent">
            <h2>What is Car Advertising</h2>
            <p>We're all familiar with outdoor advertising, we see it on billboards, buses, buildings, and on the side of the road. Outdoor advertising is something that many of us experience every day, but up until now it has been almost exclusively limited to traditional billboards.</p>

            <blockquote>Car advertising is a new form of outdoor advertising. In principle it is no different from any other outdoor format in that it is another opportunity for brands to connect with consumers. To join our fleet Car owners can register their car with Car Quids to get matched with an advertiser.</blockquote>
            
            <p>Advertising on cars gives brands the opportunity to reach new audiences. By their very nature personal vehicles are 'personal', they're part of local neighborhoods, workplaces, and retail venues, and as a result have the power to weave messaging into the fabric of everyday life in a distinctive and impactful way. This can be very powerful for brands trying to reach a local audience, whether its a national campaign targeting consumers at a community level, or regional campaigns targeting defined customer groups in specific towns and cities.</p>
            
            <p>While Car ads can reach a local audience they also get coverage in prime locations, like city centres, motorways, and major retail locations where they compete for eyeballs with existing outdoor media, like billboards, buses, taxis, and digital screens (DOOH). This combination of busy commuter traffic, retail and shopping exposure, and time spent in local neighborhoods make car advertising a very powerful medium for reaching audiences at the right time and in the right place. Uniquely car advertising is often the first and last advertising that a consumer sees, and this is essential for building and retaining mindshare</p>
            
            <p>Our fleet covers over 100 cities and towns in the UK and this is growing every single day. For more information on what car advertising can do check out our brands page.</p>
            <h2>Saving money on your vehicle – Car advertising</h2>
            <p>With the cost of owning a car skyrocketing there are a number of ways that you can save money on your vehicle that don't involve leaving your job and becoming a taxi driver. In an ongoing piece on how to save money on your car, we'll be writing weekly on different products, services, and ideas that can help you make meaningful savings on owning a car.</p>

            <p>Vehicular advertising has been around for a long time. We've all seen it on buses and other commercial vehicles, but did you know that you too can earn money for having ads on your car? As the number one player in this market CarQuids, connects drivers with advertisers. Advertisers then select where and who they want to advertise with, and the selected drivers can then earn up to £100 per month just for having ads on their cars.</p>
            
            <p>Advertising campaigns run from 3months right up to 18 months, which means by signing up with Car Quids you could guarantee yourself earnings of £100 per month for 18months, £1800 in total. Advertising campaigns come in on a rolling basis, and you could find yourself earning money every month indefinitely if there is enough demand for your car! For most drivers that kind of money is enough to cover a lot of bills, and keep the costs of running a car down.</p>
            
            <p>Drivers can chose which advertisements end up on their cars, and Car Quids only ever uses top grade vinyls. If you don't mind driving around with advertising on your car, the Car Quids car advertising program is for you.</p>
  			</article>
			<!-- /Article -->

			<!-- Sidebar -->
			<aside class="col-md-4 sidebar sidebar-right">

				<div class="row panel">
					<div class="col-xs-12">
						<h3>Car Advertising Success Stories</h3>
						<p>Foxtons is known as one of London's most well recognised estate agents. There are a number of reasons that Foxtons remains a market leader, with one of the most well known being their consistent commitment to brand building.</p>

                        <p>Visit a Foxtons branch and you'll be struck by the high-end branding and attention to detail, it's an experience that customers remember, and one that stands for quality and a high standard of service. However, to take the brand out of the branches and into the streets, Foxtons relies on car advertising to build awareness and target new customers.</p>
                        
                        <p><img src="assets/images/about.jpg" alt=""></p>
                        
                        <p>If you live in London you will almost certainly have come across a Foxtons car, and it is for this reason that car advertising has proven so effective. Unlike Billboards, car advertising has the unique ability to penetrate neighborhoods and target demographics that other outdoor advertising formats find hard to reach. Couple the wide coverage of car advertising with eye catching graphics and clear messaging, and you have a winning formula for brand building which Foxtons has executed very successfully.</p>
                        
                        <p>While this sort of anecdotal evidence demonstrates that car advertising yields extremely powerful results, recent research from 3M also shows that car advertising has advantages over other outdoor formats, with 96% of survey respondents saying they thought fleet graphics were more effective than billboards. It is to little wonder then that brands like Foxton's continue to invest in car advertising as a tool to grow their brand and target specific audiences.</p>
					</div>
				</div>
			</aside>
			<!-- /Sidebar -->
		</div>
	</section>
	<!-- /container -->