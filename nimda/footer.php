<footer id="footer">
	<div class="footer2">
			<div class="container">
				<div class="row">
                
					<!--<div class="col-md-6 panel">
						<div class="panel-body">
							<p class="simplenav">
								<a href="index.php?active=HO">Home</a> | 
								<a href="faq.php?active=FA">FAQ</a> |
								<a href="blog.php?active=BL">Blog</a> |
								<a href="contact.php?active=CO">Contact</a> |
								<a href="register.php?active=RE">Register Your Car</a>
							</p>
						</div>
					</div>-->

					<div class="col-md-12 panel">
						<div class="panel-body">
							<p align="center">
								Copyright &copy; VSL Technologies Pvt Ltd, 2014.
							</p>
						</div>
					</div>
                    
				</div>
				<!-- /row of panels -->
			</div>
		</div>
	</footer>

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script type='text/javascript' src="../assets/js/modernizr-latest.js"></script> 
	<script type='text/javascript' src='../assets/js/jquery.min.js'></script>
    <script type='text/javascript' src='../assets/js/fancybox/jquery.fancybox.pack.js'></script>
    <script type='text/javascript' src='../assets/js/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='../assets/js/jquery.easing.1.3.js'></script> 
    <script type='text/javascript' src='../assets/js/camera.min.js'></script> 
    <script type='text/javascript' src="../assets/js/bootstrap.min.js"></script> 
	<script type='text/javascript' src="../assets/js/custom.js"></script>
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.4"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		jQuery('.fancybox').fancybox();
		});
	</script>
    <script>
		jQuery(function(){
			
			jQuery('#camera_wrap_4').camera({
                transPeriod: 500,
                time: 3000,
				height: '600',
				loader: 'false',
				pagination: true,
				thumbnails: false,
				hover: false,
                playPause: false,
                navigation: false,
				opacityOnGrid: false,
				imagePath: '../assets/images/'
			});

		});
      
	</script>
    
</body>
</html>