 	<header id="head" class="secondary">
            <div class="container">
                    <h1>Frequently Asked Questions</h1>
                </div>
    </header>


    <!-- container -->
    <section class="container">
        <div class="row">
            <!-- main content -->
            <div class="col-sm-12">
                <h3>1 . How much can I earn advertising on my own car?</h3>
                <p>It depends on the advertisers that you are matched with as well as the size and type of advert applied to your car. Typically you can earn between £50 and £100 per month.</p>
                <h3>2 . Can I choose which advertisements I have on my car?</h3>
                <p>Absolutely. We would never want you to display an advert that you are not comfortable or happy with. You have the right to refuse a particular brand or advertisement if you do not want it on your car.</p>
                <h3>3 . What happened to the lease scheme?</h3>
                <p>We have paused the leasing scheme indefinitely so that means to sign up you will need your own car, or one that you have permission to use with Car Quids. To be notified if the lease scheme returns, please email us with your details at lease@carquids.com</p>
                <h3>4 . Is it free to register?</h3>
                <p>Yes, 100%. Our income is from the brands and advertisers that want to advertise on your car, not from car owners.</p>
                <h3>5 . How long does it take to get matched with an advertiser after I've registered?</h3>
                <p>This depends on whether we have an advertiser looking for drivers that match your profile. As soon as there is a potential match, we will be in touch. We actively identify opportunities where we have many members with similar profiles and approach advertisers with tailored proposals.</p>
                <h3>6 . How are adverts applied to my car? Who applies them?</h3>
                <p>Most advertising campaigns will consist of vinyl stickers on your car (usually the bonnet and both sides). We arrange for you to get them fitted with one of our partner fitters nearby.</p>
                <h3>7 . Can I update my driver profile?</h3>
                <p>Sure, just enter your email here and we'll send you an invite to update your profile. Once you submit your request you'll have 30 minutes to update your profile.</p>
                <h3>8 . How do I get in touch?</h3>
                <p>Give us a buzz or drop us an email: contact details.</p>
            </div>
        </div>
    </section>