<script language = "Javascript">
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;
var cCode = 0;
function isInteger(s)
{ var i;
for (i = 0; i < s.length; i++)
{ 
var c = s.charAt(i);
if (((c < "0") || (c > "9"))) return false;
}
return true;
}
function trim(s)
{ var i;
var returnString = "";
for (i = 0; i < s.length; i++)
{ 
var c = s.charAt(i);
if (c != " ") returnString += c;
}
return returnString;
}
function stripCharsInBag(s, bag)
{ var i;
var returnString = "";
for (i = 0; i < s.length; i++)
{ 
var c = s.charAt(i);
if (bag.indexOf(c) == -1) returnString += c;
}
return returnString;
}
function checkInternationalPhone(strPhone){
var bracket=3
strPhone=trim(strPhone)
if(strPhone.indexOf("+")>1) return false
if(strPhone.indexOf("-")!=-1)bracket=bracket+1
if(strPhone.indexOf("(")!=-1 && strPhone.indexOf("(")>bracket)return false
var brchr=strPhone.indexOf("(")
if(strPhone.indexOf("(")!=-1 && strPhone.charAt(brchr+2)!=")")return false
if(strPhone.indexOf("(")==-1 && strPhone.indexOf(")")!=-1)return false
s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}
</script>
<script type="text/javascript">
function validate()
{
	var valid=document.form;
	var iChars = "{}#^=|'<>~`\$@*;";
	var erColor="#FBFFFF";
	var digits = "0123456789";
	var Phone=document.form.phone;
			
	if(IsEmpty(valid.name))
	{
		alert("Please enter your name");
		valid.name.style.background= erColor;
		valid.name.focus();
		return false;
	}
	else
	{
		valid.name.style.background= 'White';
		valid.name.focus();
	}
	for (var i = 0; i < valid.name.value.length; i++)
	{
		if (iChars.indexOf(valid.name.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.name.style.background= erColor;
			valid.name.focus();
			return false;
		}
	}
	for (var i = 0; i < valid.name.value.length; i++)
	{
		if (digits.indexOf(valid.name.value.charAt(i)) != -1)
		{
			alert ("Please avoid numbers");
			valid.name.style.background= erColor;
			valid.name.focus();
			return false;
		}
	}
	if(IsEmpty(valid.email))
	{
		alert("Please enter your email");
		valid.email.style.background= erColor;
		valid.email.focus();
		return false;
	}
	else
	{
		valid.email.style.background= 'White';
		valid.email.focus();
	}
	if (valid.email.value!="")
	{
		if (valid.email.value.match(/[a-zA-Z0-9]+\@[a-zA-Z0-9-]+(\.(a-zA-Z0-9]{2}|[a-zA-Z0-9]{2}))+/)==null)
		{
			alert ("Please enter a valid email-address");
			valid.email.style.background= erColor;
			valid.email.focus();
			return false;
		}
		else
		{
			valid.email.style.background= 'White';
			valid.email.focus();
		}
	}
	if(Phone.value!='')
	{
		if (checkInternationalPhone(Phone.value)==false)
		{
			alert("Please enter a valid contact number");
			Phone.style.background=erColor;
			Phone.focus();
			return false;
		}
	}
	else
	{
		Phone.style.background= 'White';
		Phone.focus();
	}
	if(IsEmpty(valid.subject))
	{
		alert("Please enter your subject");
		valid.subject.style.background= erColor;
		valid.subject.focus();
		return false;
	}
	else
	{
		valid.subject.style.background= 'White';
		valid.subject.focus();
	}
	for (var i = 0; i < valid.subject.value.length; i++)
	{
		if (iChars.indexOf(valid.subject.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.subject.style.background= erColor;
			valid.subject.focus();
			return false;
		}
	}
	for (var i = 0; i < valid.subject.value.length; i++)
	{
		if (digits.indexOf(valid.subject.value.charAt(i)) != -1)
		{
			alert ("Please avoid numbers");
			valid.subject.style.background= erColor;
			valid.subject.focus();
			return false;
		}
	}
	if(IsEmpty(valid.message))
	{
		alert("Please enter your message");
		valid.message.style.background= erColor;
		valid.message.focus();
		return false;
	}
	else
	{
		valid.message.style.background= 'White';
		valid.message.focus();
	}
	for (var i = 0; i < valid.message.value.length; i++)
	{
		if (iChars.indexOf(valid.message.value.charAt(i)) != -1)
		{
			alert ("Please avoid special charcters");
			valid.message.style.background= erColor;
			valid.message.focus();
			return false;
		}
	}
	function IsEmpty(obj)
	{
		var objValue;
		objValue = obj.value.replace(/\s+$/,"");
		if(objValue.length == 0)
		{return true;}
		else{return false;}
	}
}
</script>

<header id="head" class="secondary">
	<div class="container">
		<h1>Contact Us</h1>
	</div>
</header>
<?php
include("db.php");
if($_POST['submit']!='')
{
$name=$_POST['name'];
$email=$_POST['email'];
$phone=$_POST['phone'];
$subject=$_POST['subject'];
$message=$_POST['message'];
$date=date("d-m-Y");
$qry="insert into feedback (Name,Email,Phone,Subject,Message,Date) values('$name','$email','$phone','$subject','$message','$date')";
$res=mysql_query($qry);
printf("<script>location.href='contact.php?active=CO&success=1'</script>");
} 
?>
	<!-- container -->
	<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h3 class="section-title">Feedback Form</h3>
                        <?php
                        if($_REQUEST['success']==1)
                        {
                        echo  '<div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Your feedback sent successfully!!
                        </div>';
                        }
                        ?>
						<form name="form" method="post" action="contact.php?active=CO" class="form-light mt-20" role="form" onsubmit="return validate();">
							<div class="form-group">
								<label>* Name</label>
								<input name="name" type="text" class="form-control" placeholder="Your name">
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>* Email</label>
										<input name="email"  type="text" class="form-control" placeholder="Email address">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Phone</label>
										<input name="phone"  type="text" class="form-control" placeholder="Phone number">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>* Subject</label>
								<input name="subject"  type="text" class="form-control" placeholder="Subject">
							</div>
							<div class="form-group">
								<label>* Message</label>
								<textarea name="message" class="form-control" id="message" placeholder="Write you message here..." style="height:100px;"></textarea>
							</div>
							<input name="submit" type="submit" class="btn btn-two" value="Send message"><p><br/></p>
						</form>
					</div>
					<div class="col-md-4">
						<div class="row">
								<h3 class="section-title">&nbsp;&nbsp;&nbsp;Office Address</h3>
                                <div class="col-md-7">
									<div class="contact-info">
                                        <h5>Address</h5>
                                        <p>VSL Technologies Pvt Ltd,<br />
                                        ECR, Vennangupattu,<br />
                                        Cheyyur Taluk,<br />
                                        Kanchipuram District, <br />
                                        Tamilnadu, <br />
                                        India - 603 304.</p>
                                	</div>
                                </div>
                                <div class="col-md-5">
									<div class="contact-info">
                                        <h5>Email</h5>
                                        <p>info@paymeads.in</p>
                                        <h5>Phone</h5>
                                        <p>044- 27526020</p>
								</div>
							</div> 
						</div> 	
                        <div class="row">
                        <div class="col-md-12">
                        <iframe width="100%" height="200" frameborder="1" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;q=VSL+Technologies+Pvt+Limited,+E+Coast+Rd,+Vennangupattu,+Kanchipuram,+Tamil+Nadu+603304&amp;aq=&amp;sll=13.03408,80.206921&amp;sspn=0.368593,0.631027&amp;t=h&amp;ie=UTF8&amp;geocode=FSrqugAdU1XEBA&amp;split=0&amp;hq=&amp;hnear=VSL+Technologies+Pvt+Limited,+E+Coast+Rd,+Vennangupattu,+Kanchipuram,+Tamil+Nadu+603304&amp;z=14&amp;ll=12.249642,79.975763&amp;output=embed"></iframe></div>
                        </div>					
					</div>
				</div>
			</div>
	<!-- /container -->