	<!-- Header -->
	<header id="head">
		<div class="container">
             <!--<div class="heading-text ">							
							<a class="btn btn-lg" href="register.php?active=RE" style="text-decoration:none; margin-bottom:90px">Register Your Car</a>
						</div>-->
					<div class="fluid_container">                       
                    <div class="camera_wrap camera_emboss pattern_1" id="camera_wrap_4">
                        <div data-thumb="assets/images/slides/thumbs/img4.jpg" data-src="assets/images/slides/img4.jpg"></div> 
                        <div data-thumb="assets/images/slides/thumbs/img3.jpg" data-src="assets/images/slides/img3.jpg"></div>
                        <div data-thumb="assets/images/slides/thumbs/img2.jpg" data-src="assets/images/slides/img2.jpg"></div> 
                        <div data-thumb="assets/images/slides/thumbs/img1.jpg" data-src="assets/images/slides/img1.jpg"></div> 
                    </div><!-- #camera_wrap_3 -->
                </div><!-- .fluid_container -->
		</div>
	</header>
	<!-- /Header -->

<section class="news-box top-margin">
        <div class="container">
            <h2><span>How does it work?</span></h2>
            <div class="row">
       
                <div class="col-lg-4 feature-wrap" align="center">
                    <i class="fa fa-car"></i>
                    <h4>Drivers sign up to Pay Me Ads</h4>
                    <p>Fill in the short sign up form here. We ask for a few details about your car, lifestyle and driving habits so that we can match you to appropriate advertisers.</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 feature-wrap" align="center">
                    <i class="fa fa-heart"></i>
                    <h4>The matching process begins</h4>
                    <p>We find advertisers that want to advertise on cars like yours and let you know as soon as there is a match. If you agree to go ahead, we will arrange for the adverts to be stuck on to your car.</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 feature-wrap" align="center">
                    <i class="fa fa-money"></i>
                    <h4>Drivers earn money</h4>
                    <p>Once you're advertising, we deposit money each month into your bank account and you are free to spend it on what you like!</p>
                </div>
            </div>
        </div>
</section>
   
<section class="container">
<div class="row">
<div class="col-md-12"><div class="title-box clearfix "></div><h2 class="title-box_primary">Our Team</h2></div> 
<div class="col-md-3 member-img team-member" align="center">
<img class="img-responsive img-thumbnail" src="assets/images/team/man1.jpg" alt="raman">
<h4>Kothanda Raman</h4>
<span class="pos">Director</span>
</div> 
<div class="col-md-2 member-img team-member" align="center">
<img class="img-responsive img-thumbnail" src="assets/images/team/man2.jpg" alt="geetha">
<h4>Geetha. S</h4>
<span class="pos">VP</span>
</div> 
<div class="col-md-2 member-img team-member" align="center">
<img class="img-responsive img-thumbnail" src="assets/images/team/man3.jpg" alt="muthu">
<h4>Muthukumar. S</h4>
<span class="pos">Technical Team Leader</span>
</div>  
<div class="col-md-2 member-img team-member" align="center">
<img class="img-responsive img-thumbnail" src="assets/images/team/man5.jpg" alt="gajendiran">
<h4>Gajendiran. D</h4>
<span class="pos">Software Engineer</span>
</div> 
<div class="col-md-3 member-img team-member" align="center">
<img class="img-responsive img-thumbnail" src="assets/images/team/man6.jpg" alt="praveen">
<h4>Praveenkumar. M</h4>
<span class="pos">Pre-Sales Engineer</span>
</div> 
</div>
</section>
	
<section class="container">
	<div class="row">
		<section class="col-sm-12 maincontent">
			<h2><span>About Us</span></h2>
                <img src="assets/images/about.jpg" alt="" class="img-responsive img-rounded pull-right" width="300">
                <p>paymeads.in is supported by its advertisers. These advertisers promote their product on your vehicle. Your vehicle is selected based on the application you submit.</p> 
                <p>At this time, most of our advertisement opportunities are for rear window advertisements that you will apply yourself with the help of our how-to instructions. If you are chosen as a driver, we will communicate with you through email about the opportunity to see if you would be a good fit with the product and advertiser.</p>
                <p>A rear window advertisement typically pays between $50+ per month. If your entire vehicle is wrapped, you can get paid up to $400 a month. </p>
                <p>Take your time when filling out the application as this information will be used to select the drivers during our campaigns.It doesn't matter what you do for a living, you can earn money while you drive! Make sure you accurately fill out your driver application and we will do our best to match you up with an advertiser.</p>
		</section> 
	</div>
</section>